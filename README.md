# Asus.com file scraper
Asus could remove their firmware updates from their site one day. This tool makes it possible to create a backup in case that happens.


## Install
Some of these dependencies might not be needed, but I don't know which ones if so. According to some forum post they were needed to prevent chromium-chromedriver from crashing, which is running in headless mode to fetch the webpages.
```
sudo apt install -y gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget python3-pip chromium-chromedriver
pip3 install -r requirements.txt
chmod +x ./scrape.py
./scrape.py
```
