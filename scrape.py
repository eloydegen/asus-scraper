#!/usr/bin/env python3
from bs4 import BeautifulSoup
import re
from urllib.parse import urlparse
import sys
import signal
import os
import urllib.request, urllib.error, urllib.parse
from requests_html import HTMLSession
import json

def main():

    session = HTMLSession()

    # Multiple API endpoints:
    # "GetPD" + 
    # - Drivers
    # - Manual
    # - OS: osid 32 = Android

    # TODO construct url from model list, get osid from generic url
    url_list = [
        'https://www.asus.com/support/api/product.asmx/GetPDDrivers?website=global&model=ASUS ZenPad 10 (Z301MF)&cpu=&osid=32',
        'https://www.asus.com/support/api/product.asmx/GetPDDrivers?website=global&model=ASUS+Transformer+Pad+TF300T&cpu=&osid=32',
        'https://www.asus.com/support/api/product.asmx/GetPDDrivers?website=global&model=ASUS+Transformer+Pad+Infinity+TF700T&cpu=&osid=32',
        'https://www.asus.com/support/api/product.asmx/GetPDManual?website=global&model=ASUS+Transformer+Pad+Infinity+TF700T&cpu=&osid=32' 
    ]
    urls = []
    for i in url_list:
        print(i)
        r = session.get(i)
        r.html.render()
        raw = r.html.raw_html
        raw = raw.decode('utf-8')

        # remove some HTML strings so it can be parsed as JSON
        raw = raw.replace("""<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">""", '')
        raw = raw.replace("</pre></body></html>", '')
        
        # returns the download URLs as a list
        new_urls = get_urls_from_json(json.loads(raw))
        urls = urls + new_urls
        
    for i in urls:
        print(i)
        o = urlparse(i).path
        # Split the path on the last slash, so filename is separated 
        x = o.rpartition("/")
        # First item in tuple yields the path
        path = x[0]
        # Remove the root slash
        path = path[1:]

        # (Re-)create directory structure
        os.makedirs(path, exist_ok=True)
        
        # Split the full URL on the domain + "/", then get everything after that which is the filepath
        urllib.request.urlretrieve(i, filename=i.split(urlparse(i).netloc + "/", 1)[1])

# Note: input is not formatted as JSON but dict. Original source is JSON
def get_urls_from_json(data):
    urls = []

    # loop over JSON returned from API and get nested download links 

    # first, get the length of the list that contains different categories (PC software, 
    # firmware updates and source code and maybe more), then get the lenght of items in 
    # those categories and loop through both 
    for i in range(len(data['Result']['Obj'])):
        for j in range(len(data['Result']['Obj'][i]['Files'])):
            urls.append(data['Result']['Obj'][i]['Files'][j]['DownloadUrl']['Global'])

    return urls

if __name__ == '__main__':
    main()
